package com.example.vrlab.myapplication;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    SensorEventListener mSensorListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
        public void onSensorChanged(SensorEvent event) {
            if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
                return;
            String output = "";
            float[] v = event.values;
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ORIENTATION:
                    output +=
                            "\n azimuth:" + v[0] +
                                    "\n pitch:" + v[1] +
                                    "\n roll:" + v[2] + "\n";
                    TextView txt1 =(TextView) findViewById(R.id.editText1);
                    txt1.setText(output);
                    break;
                case Sensor.TYPE_GRAVITY:
                    output +=
                            "\n X:" + v[0] +
                                    "\n Y:" + v[1] +
                                    "\n Z:" + v[2] + "\n";
                    TextView txt2 =(TextView) findViewById(R.id.editText2);
                    txt2.setText(output);
                    break;
                case Sensor.TYPE_ACCELEROMETER:
                    output +=
                            "\n X:" + v[0] +
                                    "\n Y:" + v[1] +
                                    "\n Z:" + v[2] + "\n";
                    TextView txt3 =(TextView)findViewById(R.id.editText3);
                    txt3.setText(output);
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    output +=
                            "\n X:" + v[0] +
                                    "\n Y:" + v[1] +
                                    "\n Z:" + v[2] + "\n";
                    TextView txt4 =(TextView)findViewById(R.id.editText4);
                    txt4.setText(output);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    output +=
                            "\n X:" + v[0] +
                                    "\n Y:" + v[1] +
                                    "\n Z:" + v[2] + "\n";
                    TextView txt5 =(TextView)findViewById(R.id.editText4);
                    txt5.setText(output);
                    break;
                case Sensor.TYPE_ROTATION_VECTOR:
                    output +=
                            "\n X:" + v[0] +
                                    "\n Y:" + v[1] +
                                    "\n Z:" + v[2] + "\n";
                    TextView txt6 =(TextView)findViewById(R.id.editText4);
                    txt6.setText(output);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SensorManager sensors = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensors.unregisterListener(mSensorListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int delay = SensorManager.SENSOR_DELAY_UI;
        SensorManager sensors = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensors.registerListener(mSensorListener,
                sensors.getDefaultSensor(Sensor.TYPE_ORIENTATION), delay);
        sensors.registerListener(mSensorListener,
                sensors.getDefaultSensor(Sensor.TYPE_GRAVITY), delay);
        sensors.registerListener(mSensorListener,
                sensors.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), delay);
        sensors.registerListener(mSensorListener,
                sensors.getDefaultSensor(Sensor.TYPE_GYROSCOPE), delay);
    }
}
